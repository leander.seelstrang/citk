inherit:
- orchestration-base
- orchestration-hooks
- orchestration-functions

variables:
  mode: citk

  orchestration.hook.parameters:
  - - string
    - tag
  orchestration.control.explanation: This job builds all projects contained in the
    ${distribution-name} distribution in the correct order and with maximum parallelity.

  orchestration.control.parameters:
  - name: ageLimit
    kind: string
    default: none
    description: >-
      If "none", Jenkins will initiate a build for all jobs in the
      distribution (respecting dependencies).

  run-prepare: |
    ${run-hook-function}("Running prepare hook job", "${prepare-hook-name|}", parameters)
  run-finish: |
    ${run-hook-function}("Running finish hook job", "${finish-hook-name|}", parameters)

  orchestration.control.code: |
    ${orchestration.groovy.base}
    ${orchestration.groovy.scheduling}
    ${orchestration.groovy.reporting}

    // Dependency Data

    dependencies = ${jobs.dependencies/groovy}

    // Utilities

    time  = 0
    delta = 500

    running = []
    status = [:]

    def maybeScheduleBuild(name, parameters) {
      if (status[name][1]) {
        return
      }
      scheduleBuildUnlessDisabled(name, parameters)
    }

    def scheduleBuildUnlessDisabled(name, parameters) {
      job = Hudson.instance.getJob(name)
      if (job?.disabled) {
        return
      }

      scheduleBuild(name, parameters)
    }

    def scheduleBuild(name, parameters) {
      job = Hudson.instance.getJob(name)
      if (!job) {
        logMessage("  Scheduling build for \${name} failed: job not found!")
        return
      }

      logMessage("  Scheduling build for \${linkTo(job)} with parameters \${parameters}")
      parameters = parameters.collect { parameter ->
        new StringParameterValue(parameter.key, parameter.value)
      }
      parametersAction = new ParametersAction(parameters)
      run = job.scheduleBuild2(0, new Cause.UpstreamCause(build), parametersAction)
      if (run) {
        running << run
      }
    }

    def popBuild() {
      done = running.find { build -> build.isDone() }
      if (done && !done.isCancelled()) {
        running.remove(done)
        return done.get()
      }
    }

    def spin(parameters) {
      while (running) {
        build = popBuild()
        if (build) {
          logMessage("\${build.result} \${linkTo(build.project)}, \${linkTo(build)}, \${toSeconds(build.duration)} s")
          name = build.project.name
          if (!status[name]) { // For hooks
            status[name] = [ null, null ]
          }
          status[name][1] = build;
          startableDownstreams(name).each { project ->
            maybeScheduleBuild(project, parameters)
          }
          logMessage("\${running.size} build(s)")
        }

        Thread.sleep(delta)
        time += delta
      }
    }

    // Main part

    buildId = build.time.format('yyyy-MM-dd_HH-mm-ss')
    parameters = [ "tag": buildId ]

    // Prepare build status map

    status["${prepare-hook-name|dummy-prepare-hook/}"] = [ null, null ]
    dependencies
      .each { entry -> status[entry.key] = [ null, null ] }

    ageLimitString = build.buildVariableResolver.resolve("ageLimit")
    if (ageLimitString && ageLimitString != "none") {
      ageLimit = use (groovy.time.TimeCategory) { (ageLimitString as int).hours.ago }
      markSuccessfulAndRecentBuilds(ageLimit)
    }

    // Run hooks and schedule builds for "root" projects

    @{run-prepare}

    logMessage("Running build jobs")

    try {
      dependencies.findAll { entry ->
        ((!entry.value || isStartable(entry))
         && !isSuccessfulBuildAvailable(entry.key))
      }
      .each { entry -> maybeScheduleBuild(entry.key, parameters) }
      spin(parameters)

      @{run-finish}
    } finally {
      // in case of error, cancel all running/scheduled jobs
      running.each { job -> job.cancel(true) }
    }

    // Report results

    reportResultsAndMaybeFailBuild()
