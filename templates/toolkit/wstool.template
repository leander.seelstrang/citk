inherit:
- std-build-env

variables:
  natures: []

  ros: ${next-value|indigo}
  ros.install.prefix: ${next-value|${toolkit.dir}}
  ros.underlay: ${next-value|/opt/ros/${ros}}
  ros.source.underlay: |
    # source ROS underlay
    if [ -f ${ros.install.prefix}/setup.sh ] ; then
      _ROS_UNDERLAY_=${ros.install.prefix}/setup.sh
    else
      _ROS_UNDERLAY_=${ros.underlay}/setup.sh
    fi
    set +x # disable verbose output
    . \$_ROS_UNDERLAY_
    set -x

  extra-provides:
  - nature: catkin
    target: ${project-name}
    version: ${version-name}
  - '@{next-value|[]}'

  build-job.tags:
  - ros
  - wstool
  - '@{next-value|[]}'

aspects:
- name: freestyle.shell
  aspect: shell
  conditions:
    job.tags: unix
  variables:
    aspect.environment.exports: |
      export ${catkin.environment}
    aspect.cmake.options: |
      -D${cmake.options} \
    aspect.cmake.args: |
      --cmake-args \
      @{aspect.cmake.options}  --
    aspect.shell.command: |
      ${ros.source.underlay}

      @{aspect.environment.exports}

      mkdir -p src
      cd src

      rm -f .rosinstall
      wstool init
      ${rosws.commands|echo "no rosws.commands variable provided!"}
      wstool update --delete-changed-uris

      cd ..
      ${shell.pre-build}

      # Rosjava Stuff
      ROS_MAVEN_REPOSITORY=file://${toolkit.dir}/share/repository/
      ROS_MAVEN_DEPLOYMENT_REPOSITORY=${toolkit.dir}/share/repository

      catkin --no-color config --install --install-space ${ros.install.prefix} ${aspect.cmake.args}
      catkin build --force-cmake --no-status --no-notify --summarize

jobs:
- name: main
  variables:
    catkin.environment:
    - '@{shell.environment|[]}'
    - '@{next-value|[]}'

    cmake.options: ${cmake.options:${mode}|${cmake.options:default}}
    cmake.options:default:
      - CMAKE_BUILD_TYPE=${cmake.build.type|None}
      - CMAKE_CXX_FLAGS="\${CXXFLAGS}"
      - CMAKE_C_FLAGS="\${CFLAGS}"
      - '@{next-value|[]}'
