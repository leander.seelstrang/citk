# This template describes executable programs.
#
# Such a program description typically provides one or more features
# using the program nature. The target of such a provided feature is
# the name of the binary without any other path components.
#
# In some cases, there is a well-known CMake module corresponding to
# the program. In that case, the cmake feature should be provided as
# well.

minimum-generator-version: 0.32.13

variables:
  platform-provides:
  - '@{next-value|[]}'

  # Core

  - name: program-coreutils
    variables:
      extra-provides:
      - program: uname
      platform-requires:
        ubuntu:
          packages:
          - coreutils

  - name: program-grep
    variables:
      extra-provides:
      - program: grep
      platform-requires:
        ubuntu:
          packages:
          - grep

  - name: program-find
    variables:
      extra-provides:
      - program: find
      platform-requires:
        ubuntu:
          packages:
          - findutils

  - name: program-file
    variables:
      extra-provides:
      - program: file
      platform-requires:
        ubuntu:
          packages:
           - file

  - name: program-sed
    variables:
      extra-provides:
      - program: sed
      platform-requires:
        ubuntu:
          packages:
           - sed

  - name: program-lsb_release
    variables:
      extra-provides:
      - program: lsb_release
      platform-requires:
        ubuntu:
          packages:
          - lsb-release

  - name: program-sh
    variables:
      extra-provides:
      - program: sh
      platform-requires:
        ubuntu:
          packages:
          - dash

  # Version control

  - name: program-git
    variables:
      extra-provides:
      - program: git
      - cmake: git
      platform-requires:
        ubuntu:
          packages:
          - git

  - name: program-git-lfs
    variables:
      extra-provides:
      - program: git-lfs
      platform-requires:
        ubuntu:
          packages:
          - git-lfs

  - name: program-subversion
    variables:
      extra-provides:
      - program: svn
      - cmake: subversion # non-canonical, but should be harmless
      platform-requires:
        ubuntu:
          packages:
          - subversion

  - name: program-mercurial
    variables:
      extra-provides:
      - program: hg
      platform-requires:
        ubuntu:
          packages:
          - mercurial

  - name: program-wget
    variables:
      extra-provides:
      - program: wget
      platform-requires:
        ubuntu:
          packages:
          - wget

  - name: program-curl
    variables:
      extra-provides:
      - program: curl
      platform-requires:
        ubuntu:
          packages:
          - curl

  # Archives

  - name: program-unzip
    variables:
      extra-provides:
      - program: unzip
      platform-requires:
        ubuntu:
          packages:
          - unzip

  - name: program-unp
    variables:
      extra-provides:
      - program: unp
      platform-requires:
        ubuntu:
          packages:
          - unp

  - name: program-tar
    variables:
      extra-provides:
      - program: tar
      platform-requires:
        ubuntu:
          packages:
          - tar

  - name: program-bzip2
    variables:
      extra-provides:
      - program: bzip2
      platform-requires:
        ubuntu:
          packages:
          - bzip2

  - name: program-gzip
    variables:
      extra-provides:
      - program: gzip
      platform-requires:
        ubuntu:
          packages:
          - gzip

  # Packaging

  - name: program-lintian
    variables:
      extra-provides:
      - program: lintian
      platform-requires:
        ubuntu:
          packages:
          - lintian

  - name: program-dpkg
    variables:
      extra-provides:
      - program: dpkg
      platform-requires:
        ubuntu:
          packages:
          - dpkg

  # Interpreters

  - name: program-python2
    variables:
      extra-provides:
      - program: python
      - program: python2
      platform-requires:
        ubuntu:
          packages:
          - python

  - name: program-python2.3
    variables:
      extra-provides:
      - program: python2.3
      platform-requires:
        ubuntu:
          packages:
          - python2.3

  - name: program-python2.6
    variables:
      extra-provides:
      - program: python2.6
      platform-requires:
        ubuntu:
          packages:
          - python2.6

  - name: program-python3
    variables:
      extra-provides:
      - program: python3
      platform-requires:
        ubuntu:
          packages:
          - python3

  - name: program-cython
    variables:
      extra-provides:
      - program: cython
      platform-requires:
        ubuntu:
          packages:
          - cython

  - name: program-ipython
    variables:
      extra-provides:
      - program: ipython
      platform-requires:
        ubuntu:
          packages:
          - ipython

  - name: program-ruby
    variables:
      extra-provides:
      - program: ruby
      # FindRuby.cmake, included in CMake distribution
      # This CMake module checks for the program as well as the
      # library
      - cmake: ruby
      platform-requires:
        ubuntu:
          packages:
          - ruby
          - ruby-dev

  - name: program-lua51
    variables:
      extra-provides:
      - program: lua
      - cmake: lua51 # FindLua51.cmake, included in CMake distribution
      platform-requires:
        ubuntu:
          packages:
          - lua5.1

  - name: luajit2.1
    variables:
      extra-provides:
      - nature: program
        target: luajit
        version: '2.1'
      platform-requires:
        ubuntu:
          packages:
          - luajit

  - name: program-java@8
    variables:
      extra-provides:
      - nature: program
        target: java
        version: '8.0'
      platform-requires:
        ubuntu:
          packages:
          - openjdk-8-jre-headless

  - name: program-java@11
    variables:
      extra-provides:
      - nature: program
        target: java
        version: '11.0'
      platform-requires:
        ubuntu:
          packages:
          - openjdk-11-jre-headless

  # Compilers

  - name: program-gcc
    variables:
      extra-provides:
      - program: gcc
      - program: gcov
      platform-requires:
        ubuntu:
          packages:
          - gcc

  - name: program-g++
    variables:
      extra-provides:
      - program: g++
      platform-requires:
        ubuntu:
          packages:
          - g++

  - name: program-gfortran
    variables:
      extra-provides:
      - program: gfortran
      platform-requires:
        ubuntu:
          packages:
          - gfortran

  - name: program-clang
    variables:
      extra-provides:
      - nature: program
        target: clang
        version: '6.0'
      platform-requires:
        ubuntu:
          xenial:
            packages:
            - clang-6.0

  - name: program-sbcl
    variables:
      extra-provides:
      - program@build: sbcl
      - program: sbcl
      platform-requires:
        ubuntu:
          packages:
          - sbcl

  - name: program-javac@8
    variables:
      extra-provides:
      - nature: program
        target: javac
        version: '8.0'
      - cmake: JNI
      platform-requires:
        ubuntu:
          packages:
          - openjdk-8-jdk-headless

  - name: program-javac@11
    variables:
      extra-provides:
      - nature: program
        target: javac
        version: '11.0'
      - nature: cmake
        target: JNI
      platform-requires:
        ubuntu:
          packages:
          - openjdk-11-jdk-headless

  - name: program-javac@17
    variables:
      extra-provides:
      - nature: program
        target: javac
        version: '17.0'
      - nature: cmake
        target: JNI
      platform-requires:
        ubuntu:
          packages:
          - openjdk-17-jdk-headless

  - name: program-ccache
    variables:
      extra-provides:
      - program: ccache
      platform-requires:
        ubuntu:
          packages:
          - ccache

  # Build systems

  - name: program-autoconf
    variables:
      extra-provides:
      - program: autoconf
      platform-requires:
        ubuntu:
          packages:
          - autoconf

  - name: program-make
    variables:
      extra-provides:
      - program: make
      platform-requires:
        ubuntu:
          packages:
          - make

  - name: program-cmake
    variables:
      extra-provides:
      - program: cmake
      platform-requires:
        ubuntu:
          packages:
          - cmake

  - name: program-ant
    variables:
      extra-provides:
      - program: ant
      platform-requires:
        ubuntu:
          packages:
          - ant

  - name: program-maven
    variables:
      extra-provides:
      - program: mvn
      platform-requires:
        ubuntu:
          packages:
          - maven

  - name: program-meson
    variables:
      extra-provides:
      - program: meson
      platform-requires:
        ubuntu:
          packages:
          - meson

  - name: program-ninja
    variables:
      extra-provides:
      - program: ninja
      platform-requires:
        ubuntu:
          packages:
          - ninja-build

  # Code checks, reports, formatting

  - name: program-sloccount
    variables:
      extra-provides:
      - program: sloccount
      - cmake: sloccount # non-canonical, but should be harmless
      platform-requires:
        ubuntu:
          packages:
          - sloccount

  - name: program-cppcheck
    variables:
      extra-provides:
      - program: cppcheck
      - cmake: cppcheck # non-canonical, but should be harmless
      platform-requires:
        ubuntu:
          packages:
          - cppcheck

  - name: program-clang-format-6.0
    variables:
      extra-provides:
      - program: clang-format-6.0
      platform-requires:
        ubuntu:
          packages:
          - clang-format-6.0

  # Code generators

  - name: program-protocol-buffers-compiler
    variables:
      extra-provides:
      - program: protoc
      platform-requires:
        ubuntu:
          packages:
          - protobuf-compiler

  - name: program-flex
    variables:
      extra-provides:
      - program: flex
      - cmake: flex
      platform-requires:
        ubuntu:
          packages:
          - flex

  - name: program-bison
    variables:
      extra-provides:
      - program: bison
      - cmake: bison
      platform-requires:
        ubuntu:
          packages:
          - bison

  - name: program-swig
    variables:
      extra-provides:
      - program: swig
      - cmake: swig
      platform-requires:
        ubuntu:
          packages:
          - swig

  # Documentation tools

  - name: program-doxygen
    variables:
      extra-provides:
      - program: doxygen
      - cmake: doxygen
      platform-requires:
        ubuntu:
          packages:
          - doxygen

  - name: program-man
    variables:
      extra-provides:
      - program: man
      platform-requires:
        ubuntu:
          packages:
          - man-db

  - name: program-sphinx-build
    variables:
      extra-provides:
      - program: sphinx-build
      - program: sphinx-apidoc
      - cmake: sphinx

      debianoid-packages:
      - python-sphinx
      platform-requires:
        debian:
          packages:
          - python-sphinx # '@{debianoid-packages}'
        ubuntu:
          bionic:
            packages:
            - python-sphinx # '@{debianoid-packages}'
          jammy:
            packages:
            - sphinx-doc 
            - sphinx-common

  - name: program-texinfo
    variables:
      extra-provides:
      - program: texinfo
      platform-requires:
        ubuntu:
          packages:
          - texinfo

  - name: program-yard
    variables:
      extra-provides:
      - program: yard
      platform-requires:
        ubuntu:
          packages:
          - yard

  - name: program-rdoc
    variables:
      extra-provides:
      - program: rdoc
      platform-requires:
        ubuntu:
          packages:
          - ruby-sdoc

  # Other

  - name: program-ronn
    variables:
      extra-provides:
      - program: ronn
      platform-requires:
        ubuntu:
          packages:
          - ruby-ronn

  - name: program-time
    variables:
      extra-provides:
      - program: time
      platform-requires:
        ubuntu:
          packages:
          - time

  - name: program-valgrind
    variables:
      extra-provides:
      - program: valgrind
      platform-requires:
        ubuntu:
          packages:
          - valgrind

  - name: program-libtool
    variables:
      extra-provides:
      - program: libtoolize
      platform-requires:
        ubuntu:
          packages:
          - libtool

  - name: program-pkg-config
    variables:
      extra-provides:
      - program: pkg-config
      - cmake: PkgConfig
      platform-requires:
        ubuntu:
          packages:
          - pkg-config

  - name: program-pdflatex
    variables:
      extra-provides:
      - program: pdflatex
      platform-requires:
        ubuntu:
          packages:
          - texlive-latex-base

  - name: program-gnuplot
    variables:
      extra-provides:
      - program: gnuplot
      platform-requires:
        ubuntu:
          packages:
          - gnuplot-x11

  - name: program-hugo
    variables:
      extra-provides:
      - program: hugo
      platform-requires:
        ubuntu:
          packages:
          - hugo

  - name: program-xsdcxx
    variables:
      extra-provides:
      - program: xsdcxx
      platform-requires:
        ubuntu:
          packages:
          - xsdcxx

  - name: program-cppad
    variables:
      extra-provides:
      - program: cppad
      platform-requires:
        ubuntu:
          packages:
          - cppad

  - name: program-lcov
    variables:
      extra-provides:
      - program: lcov
      - cmake: lcov
      platform-requires:
        ubuntu:
          packages:
          - lcov

  - name: program-gcovr
    variables:
      extra-provides:
      - program: gcovr
      - cmake: gcovr # non-canonical, but should be harmless
      platform-requires:
        ubuntu:
          packages:
          - gcovr

  - name: program-xsltproc
    variables:
      extra-provides:
      - program: xsltproc
      platform-requires:
        ubuntu:
          packages:
          - xsltproc

  - name: program-libxml2-utils
    variables:
      extra-provides:
      - program: xmllint
      platform-requires:
        ubuntu:
          packages:
          - libxml2-utils

  - name: program-gst-launch-0.10
    variables:
      extra-provides:
      - program: gst-launch-0.10
      platform-requires:
        ubuntu:
          packages:
          - gstreamer0.10-tools

  - name: program-gst-launch-1.0
    variables:
      extra-provides:
      - program: gst-launch-1.0
      platform-requires:
        ubuntu:
          packages:
          - gstreamer1.0-tools

  - name: program-sox
    variables:
      extra-provides:
      - program: sox
      platform-requires:
        ubuntu:
          packages:
          - sox

  - name: program-mpirun
    variables:
      extra-provides:
      - program: mpirun
      platform-requires:
        ubuntu:
          packages:
          - mpi-default-bin

  - name: program-glxinfo
    variables:
      extra-provides:
      - program: glxinfo
      platform-requires:
        ubuntu:
          packages:
          - mesa-utils

  - name: program-module
    variables:
      extra-provides:
      - program: module
      platform-requires:
        ubuntu:
          packages:
          - environment-modules
