# CITK

This repository contains descriptions of projects and distributions
(which are collections of projects) alongside instructions for
building some common kinds of projects using [RDTK](https://github.com/RDTK).

You can find a `Getting Started` in the [RDTK documentation](https://rdtk.github.io/documentation/getting_started.html)

## Sub directories

`projects`

> Contains the actual project descriptions. See [Project
> Descriptions](#project-descriptions).

`distributions`

> Contains descriptions of "distributions" which are groups of project
> versions that can be built and used together. See [Distribution
> Descriptions](#distribution-descriptions).

`templates`

> Contains descriptions of common project **kinds** including how to
> build and install the respective kinds of projects depending on the
> overall development mode. See [Templates](#templates).

`assets`

> Contains markdown-based descriptions of distributions and projects.
> These extended descriptions are an addition to the description
> inside the project and distributions files.

For more information, the git repository and bug reports, visit the
[gitlab project](https://gitlab.ub.uni-bielefeld.de/rdtk/citk).

## Contributing

Your contributions are always welcome!

A successful pipeline is required for merges on the `master` branch. You can use `build-generator validate` to validate your changes locally.

### Create merge request

To apply changes to `master` you have to create a merge request. 

People with `developer` permissions can simply push to create branches on this repository.

Example: you have some local changes

```
> git status

On branch master
Your branch is ahead of 'origin/master' by 1 commit.
```

Push to new remote branch `update_docu`

```
> git push origin master:update_docu  

Total 0 (delta 0), reused 0 (delta 0)
remote: 
remote: To create a merge request for update_docu, visit:
remote:   https://gitlab.ub.uni-bielefeld.de/rdtk/citk/-/merge_requests/new?merge_request%5Bsource_branch%5D=update_docu
remote: 

```

Open the link and click the `create merge request` button then click on `merge when pipeline succeeds`