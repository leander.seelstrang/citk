## Quickstart

The current documentation is hostet on [github](https://rdtk.github.io/documentation/index.html)

The following Introduction is the from the old readme file.

#### Introduction


This repository contains descriptions of projects and distributions
(which are collections of projects) alongside instructions for
building some common kinds of projects.

Sub directories:

`projects`

> Contains the actual project descriptions. See [Project
> Descriptions](#project-descriptions).

`distributions`

> Contains descriptions of "distributions" which are groups of project
> versions that can be built and used together. See [Distribution
> Descriptions](#distribution-descriptions).

`templates`

> Contains descriptions of common project **kinds** including how to
> build and install the respective kinds of projects depending on the
> overall development mode. See [Templates](#templates).

`assets`

> Contains markdown-based descriptions of distributions and projects.
> These extended descriptions are an addition to the description
> inside the project and distributions files.

For more information, the git repository and bug reports, visit the
[gitlab project](https://gitlab.ub.uni-bielefeld.de/rdtk/citk).

#### Description Language


On the syntactic level, all descriptions are currently based on
[YAML](http://yaml.org/). Since YAML is a superset of
[JSON](http://de.wikipedia.org/wiki/JavaScript_Object_Notation),
descriptions can also be written using JSON syntax.

Useful features and caveats of the YAML syntax:

-   For shell scripts and fragments of shell code, "literal style
    scalar nodes" can be useful:

        command: |
          if [ -r "\${file}" ] ; then
            frob "\${file}" 'foo bar' \
            && fez
          fi

    Note how double quotes (`"`), single quotes (`'`) and backslashes
    (`\`) can be used freely without escaping while `\${…}` is needed
    to prevent the generator from applying its variable
    substitution. The `|` on the previous line indicates that the
    value should be taken literally which means whitespace and
    newlines are kept without modification. Any common whitespace
    (that is preceding all lines of the value) is stripped. Thus, the
    above example would be processed as a string consisting of the
    following four lines:

        if [ -r "\${file}" ] ; then
          frob "\${file}" 'foo bar' \
          && fez
        fi

-   Due to the fact that YAML does not generally require delimiters
    around strings, a little bit of care has to be taken when
    specifying versions. The potential problem is that something like
    `master` is parsed as a string while something like `1.0` is
    parsed as a floating point number by default. For this reason,
    certain version numbers have to be specified as `"`- or
    `'`-delimited strings:

        no-problem:       master
        still-no-problem: 1.0.0
        bad:              1.0
        good:             '1.0'

-   As an extension to core YAML syntax, recipes can include other
    YAML documents as well as text files:

    -   A tagged scalar node of the form `!b!include FILENAME` causes
        `FILENAME` to be loaded as a YAML file and spliced into the
        current recipe, replacing the tagged scalar node. Example:

            packages:
              !b!include package-list.yaml

    -   A tagged scalar node of the form `!b!literal-include FILENAME`
        causes `FILENAME` to be read as a string and spliced into the
        current recipe, replacing the tagged scalar node. Example:

            patch: !b!literal-include patches/foo.diff
            command: |
              cat <<'EOF' | patch -l p1
              ${patch}
              EOF


#### Variables and the Substitution Sub-Language


Objects such as projects, distributions and templates described within
this repository are basically named entities with a dictionary of
variable names and their values. In YAML, this translates into:

    …

    variables:
      NAME1: VALUE1,
    …

The value part of a variable definition is either a Boolean, a number,
a string or a list of values. A small substitution sub-language,
syntactically similar to "parameter expansion" in UNIX shells, can be
used to refer to values of other variables. To this end we distinguish
`${}` and `@{}` substitution, which act differently on strings and
lists.

Some examples of variable definitions:

    list.a:  [ "abc", "def" ]
    list.b:  [ "123", "456" ]
    list.c:  [ "@{list.a}", "@{list.b}", "uvw", ${text} ]
    text:    "xyz"

    dashed:  "${list.c}-"
    product: "(${list.a} * ${list.b})"

The basic syntax is as follows:

-   Scalar substitution

    `${NAME}`

    > Expands to the value of the variable named `NAME`.
    >
    > Lists stay lists. However, ultimately a list is flattened into a
    > string by concatenating all quoted items. In order to join with
    > a specific delimiter character, e.g. a space, define a new variable
    > adding the appropriate suffix beforehand: `"${NAME} "`

    `${NAME|}`

    `${NAME|DEFAULT}`

    > Expands to the value of the variable named `NAME` if there is one
    > and `DEFAULT` otherwise.

    `${next-value|DEFAULT}`

    > Expands to the value of the variable this definition is used in as
    > inherited from a parent scope (if there is a value) and `DEFAULT`
    > otherwise. As before, `DEFAULT` can be empty or omitted entirely.

-   List substitution

    Concatenates the elements of a list into a single string if there is
    surrounding text. Otherwise, i.e. if there is no surrounding text,
    it evaluates to the first element of the list. Obviously, this only
    applies to list variables.

    `@{NAME}`

    > In string context, expands to the first item of list variable
    > `NAME`.
    >
    > In list context, expands to the list of items of list variable
    > `NAME`, which allows to extend lists this:
    >
    >     [ "@{list.a}", "@{list.b|[]}", "new item" ]

    `TEXT1@{NAME}TEXT2`

    > Expands to the string concatenated from `TEXT1`, elements of list
    > variable `NAME` and `TEXT2`.

    `@{NAME|[]}`

    > Expands to the content of the variable `NAME` if it is defined or
    > the empty string/list otherwise (as indicated by `[]`). If there
    > is surrounding text

    `@{next-value|[]}`

    > expands to the value of the variable this definition is used in as
    > inherited from a parent scope (if there is a value) and the empty
    > list otherwise. As before, `[]` can omitted.

-   Products

    If the evaluation of a variable results in a list, this list is
    "multiplied" with the rest of the value string.

-   Examples

    Assuming the following variables are defined (repeated from above):

        list.a:  [ "abc", "def" ]
        list.b:  [ "123", "456" ]
        list.c:  [ "@{list.a}", "@{list.b}", "uvw", ${text} ]
        text:    "xyz"
        dashed:  "${list.c}-"
        product: "(${list.a} * ${list.b})"

    , the following results would be produced:

        "${list.a}"           => [ "abc", "def" ]
        "@{list.a}"           => "abc"
        "@{list.a} foo"       => "abcdef foo"

        "@{list.a} @{list.b}" => "abcdef 123456"
        "@{list.c|[]} "       => "abcdef123456uvwxyz"

        "${dashed}"           => [ "abc-", "def-", "123-", "456-", "uvw-", "xyz-" ]
        "@{dashed} "          => "abc-def-123-456-uvw-xyz-"

        "${product}"          => [ "(abc * 123)", "(abc * 456)", "(def * 123)", "(def * 456)" ]
        "@{product} "         => "(abc * 123)(abc * 456)(def * 123)(def * 456)"

Like in UNIX shells, substitutions are performed recursively, that is
`${${NAME}}` expands to the value of variable named by the value of the
variable `NAME`. The same is true for `DEFAULT`.

#### Variable Scopes


Different objects in the description language like projects or
distributions have separate variable sections. However, all these
sections are effectively considered when generating a build job and
thereby resolving to the values of required variables. This resolution
process is determined by the parent-child relation of the different
objects. This relation is as follows (`->` indicates that the left side
is a parent of the right side):

    DISTRIBUTION -> PROJECT -> VERSION -> JOB[including template]

When a variable needs to be resolved the resolution process iterates
from the most specific object (e.g. a `JOB`) to the most general object
until the first definition is found. This definition sets the variable
value. In case this definition contains a `next-value` (see above), the
iteration continues on the next level to resolve the `next-value` part.

Templates define variables at various levels (project, job, aspects).
Consequently, the following order of variable resolution results:

    command line -> distribution -> template variables -> project -> version -> template job variables -> template aspect variables

#### Access and Credentials


[Project Descriptions](#project-descriptions) and [Distribution
Descriptions](#distribution-descriptions) can specify whether the
described distribution or project is publicly accessible.  For a
publicly accessible distribution or project, the entire associated
source code can be obtained without any authentication.

Conversely, when project source code has to be obtained from a
repository that requires authentication, the necessary authentication
can be configured.

For access and credentials specification, the following variables are
important:

`variables` » `access`

> Specifies accessibility of the project or distribution. If present,
> has have one of the values `private` and `public`. Omitting the
> variable is equivalent to `public`.

`variables` » `scm.credentials`

> The value of this variable names an entry in Jenkins' global
> credentials store that should be associated to the repository of this
> project in generated Jenkins jobs.
>
> The name specified in the recipe must match the `ID` in Jenkins (only
> visible after `Advanced` button was pressed).

The following rules apply:

-   Projects with `scm.credentials` entries must specify `private`
    `access`.
-   Distributions with one or more `private` projects must specify
    `private` `access`


#### Project Descriptions


Project descriptions include the minimal amount of information required
to reliably build a software project:

-   Name of the project
-   "Kind" of the project (e.g. CMake, Python setuptools, …)
-   Repository URL
-   Specific build instructions if different from default build
    instructions for the project's kind. Moreover, customizations are
    possible.

A basic project description, which in this case would be stored in a
file named `rsb-cpp.project`, looks like this:

    catalogue:
    - title: Robotics Service Bus

    templates:
    - code-corlab
    - cmake-cpp
    - base

    variables:
      description: C++ implementation of the robotics service bus (RSB)
      keywords:
      - middleware
      - integration
      - robotics

      recipe.maintainer:
      - Some One <someone@techfak.uni-bielefeld.de>
      - Someone Else <someoneelse@techfak.uni-bielefeld.de>

      redmine-project: rsb

      repository: https://code.cor-lab.de/git/rsb.git.cpp
      branches:
      - master
      - "0.9"

      platform-requires:
        ubuntu:
          packages:
          - libboost-date-time-dev
          …

      cmake.options:
      - BUILD_TESTS=OFF
      - BUILD_EXAMPLES=OFF
      - "@{next-value|[]}"

The following fields are most important:

filename (not a field)

> The filename (without the `.project` extension) defines the name of
> the project. Since it is a filename, it is implicitly unique
> w.r.t. to all other projects in the repository. Should in general
> not contain any version information (unless particular versions of a
> project genuinely constitute their own projects).

`templates`

> A list of [template](#templates) names characterizing the technical
> and organizational nature of the project. For most basic purposes,
> it is sufficient to choose a suitable [template](#templates) for the
> build system used by the project and the `base`
> [template](#templates) to inherit specifications for the general
> nature of build jobs to create.
>
> It is important to note that templates first mentioned have higher
> precedence for defining variables than templates further to the end of
> the list. Therefore, the `base` template should always be listed last
> so that more specific templates can override variables defined in the
> base template.

`variables` » `repository`

> URL of the SCM repository from which a copy of the project can be
> obtained.

`variables` » `branches`

> List of branches within the SCM repository of the project which
> should be "considered".

`variables` » `description`

> A rather short description of the project can be multiple lines or
> even paragraphs.

`variables` » `maintainer`

> A list of people maintaining the corresponding project (names, or
> even better: email addresses)

`variables` » `recipe.maintainer`

> A list of people maintaining the recipe (names, or even better:
> email addresses)

`variables` » `keywords`

> A list of keyword characterizing the project.

`variables` » `platform-requires` » `OPERATING-SYSTEM-TYPE` »
`packages`

> A list of packages that have to be installed when building on an
> operating system of type `OPERATING-SYSTEM-TYPE`, independent of the
> operating system version.
>
> Example values: `ubuntu`, `arch`.

`variables` » `platform-requires` » `OPERATING-SYSTEM-TYPE` »
`{OPERATING-SYSTEM-VERSION}` » `packages`

> A list of packages that have to be installed when building on an
> operating system of type `OPERATING-SYSTEM-TYPE` with version
> `OPERATING-SYSTEM-VERSION`. Merged with version-independent
> package-requirements for `OPERATING-SYSTEM-TYPE`.
>
> Example values: for `OPERATING-SYSTEM-TYPE` `ubuntu`: `precise`,
> `trusty`.

`variables` » `platform-requires` » `OPERATING-SYSTEM-TYPE` »
`OPERATING-SYSTEM-VERSION` » `ARCHITECTURE` » `packages`

> A list of packages that have to be installed when building on a
> `ARCHITECTURE` machine with an operating system of type
> `OPERATING-SYSTEM-TYPE` with version `OPERATING-SYSTEM-VERSION`.
> Merged with the previous two entries.
>
> Example values: `i686`, `x86_64`.

`catalog` » `description`

> A reference to an extended description of the project. The format is
> markdown.


##### Project Versions


Project recipes may contain an additional `versions` section which
describes individual project versions in more detail. An entry for a
version in this section supports the following keys:

`name`

> Name of the project version. The name must be unique within the
> project recipe. If `name` is specified, `pattern` must not be
> specified.

`pattern`

> A regular expression against which version names requested in
> distributions should be matched to determine whether the version
> entry can describe the requested version. By default, a sub-string
> match is performed, `^EXPRESSION$` forces a whole-string match. The
> regular expression may use capture groups (written as sub-expressions
> enclosed in parenthesis: `(CAPTURED-EXPRESSION)`) to make
> sub-strings of the requested version name available in the
> `variables` section. If `pattern` is specified, `name` must not be
> specified.

`variables`

> Variable definitions specific to the project version. These override
> project-wide definitions of variables.

`variables` » `version-name`

> If `name` is specified, the value of this variable is the value of
> `name`. If `pattern` is specified, the value of this variable is the
> string that matched the pattern.

`variables` » `match:N` (where `0 ≤ N`)

> If `pattern` is used instead of `name`, these special variables are
> bound to the capture groups of the regular expression specified as
> `pattern`. `match:0` corresponds to the entire matching string,
> `match:1`, `match:2`, … correspond to the capture groups of the
> regular expressions.
>
> For example, matching `0.15-test` against the pattern
> `^([0-9]+.[0-9]+)-(.*)$` results in bindings
>
> -   `version-name → 0.15-test`
> -   `match:0      → 0.15-test`
> -   `match:1      → 0.15`
> -   `match:2      → test`

Example:

    versions:
    - name: master
      variables:
        branch: master
        cmake.options:
        - '@{next-value}'
        - BUILD_TESTS=ON

    - pattern: ^release-(.*)$
      variables:
        tag: ${match:1}
        cmake.options:
        - '@{next-value}'
        - BUILD_TESTS=OFF


#### Distribution Descriptions


A basic distribution description, which in this case would be stored in
a file name `toolkit-nightly.distribution`, looks like this:

    variables:
      toolkit.volume: /tmp
      toolkit.dir: ${toolkit.volume}/${distribution-name}

    versions:
    - spread  @trunk
    - cbf     @0.1

The following fields are most important:

filename (not a field)

> The filename (without the `.distribution` extension) defines the
> name of the distribution. This should also include a version
> specification indicated after a `-` character.

`variables` » `toolkit.dir`

> The installation prefix for the distribution.

`versions`

> A list of project names and versions which should be part the
> distribution.
>
> When multiple versions of one project should be included, the
> following syntax has to be used for the project's entry:
>
>     - name: PROJECT
>       versions:
>       - version: VERSION₁
>       - version: VERSION₂

#### Templates


Templates describe generic properties for certain recurring aspects of
projects. These could be build system instructions or common settings
for redmine instances.

Templates are separated into distinct modes which describe completely
disparate usage purposes and therefore end up in differently configured
Jenkins build jobs. This separation is achieved through distinct folder
structures. The different folders are described in the following:

`_common`

> Contains templates that are shared by all distinct modes. This folder
> cannot be used as a generator mode. In case a specific mode uses a
> template from this folder, it is symlinked into that mode's folder.

`toolkit`

> Describes a mode that can be used to generate build jobs which are
> able to bootstrap a complete software distribution from scratch by
> installing it to a shared filesystem prefix. Therefore, the Jenkins
> jobs are orchestrated by a buildflow job which triggers the distinct
> jobs per software project in the appropriate order. No polling of SCM
> is performed in this mode and jobs have to be triggered manually via
> the Jenkins interface.

`ci-deploy`

> This mode is used to continuously update a software distribution
> installed in a shared prefix whenever an SCM change to a software
> component appeared. This mode does not handle the bootstrapping phase
> correctly as it starts building software projects whenever a change is
> detected or an upstream project's build finished.

`ci`

> Continuous integration mode for continuously building and testing
> software without installing it to a distinct filesystem prefix.
> Artifact copying is used between build job workspaces to handle
> dependency resolution.

#### Template Syntax


A typical template description looks as follows:

    inherit:
    - foo-template

    variables:
      natures:
      - cmake

    aspects:
    - name: cmake-cpp.cmake/unix
      aspect: cmake/unix
      filter: unix
      variables:
        aspect.cmake.environment: ${cmake.environment|}
        aspect.cmake.options: ${cmake.options}
        aspect.cmake.targets: ${cmake.targets}

    jobs:
    - name: toolkit
      tags:
      - toolkit
      - architecture-dependent
      - gcc
      - unix
      - linux
      variables:
        build-job-name: ${project-name}-${version-name}-toolkit-${distribution-name}
        cmake.environment:
        - PKG_CONFIG_PATH="${toolkit.dir}/lib/pkgconfig:\${PKG_CONFIG_PATH}"
        - "@{next-value|[]}"
        cmake.options:
        - CMAKE_INSTALL_PREFIX="${toolkit.dir}"

        - CMAKE_BUILD_TYPE=RelWithDebInfo

        - CMAKE_SKIP_BUILD_RPATH=FALSE
        - CMAKE_BUILD_WITH_INSTALL_RPATH=FALSE
        - CMAKE_INSTALL_RPATH="${toolkit.dir}/lib"

        - "@{next-value|[]}"
        cmake.targets:
        - install

The following fields are most important:

filename (not a field)

> The filename (without the `.template` extension) defines the name of
> the template.

`variables`

> Variables specific to this template.

`aspects`

> Aspects are specific configuration fragments to include in the
> generated Jenkins jobs.

`jobs`

> The kind of Jenkins jobs to generate for projects that have this
> template assigned.
