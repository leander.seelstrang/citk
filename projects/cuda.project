templates:
- freestyle
- base
- shell-hooks-defaults

variables:
  recipe.maintainer:
  - flier@techfak.uni-bielefeld.de
  description: cuda headers and shared objects
  keywords:
  - cuda

  extra-provides:
  - freestyle: cuda
  - nature: cmake
    target: ${project-name}
    version: ${version-name}

  extra-requires:
  - program: g++
  - library: freeglut3
  - library: x11
  - '@{next-value|[]}'

  platform-requires:
    ubuntu:
      packages:
      - build-essential
      - libxmu-dev
      - libxi-dev
      - libglu1-mesa
      - libglu1-mesa-dev

  cuda_fix_install: 'true'
  cuda_check_error: 'true'
  cuda_dl_url: "https://developer.download.nvidia.com/compute/cuda/11.1.0/local_installers/cuda_11.1.0_455.23.05_linux.run"
  cuda_versionfile: version.txt

  post-build-hook/unix.command: |
    cd ${toolkit.dir}

    if ${cuda_check_error}; then
      cat /tmp/cuda-installer.log | grep "\[WARNING\]" || true
      if cat /tmp/cuda-installer.log | grep "\[ERROR\]"; then
        errors=$(cat /tmp/cuda-installer.log | grep "\[ERROR\]")
        # We have to filter out this symlink error that happens ALWAYS
        if echo $errors | grep -q -v "boost::filesystem::read_symlink"; then
          return -1;
        fi
      fi
    fi

    if ${cuda_fix_install}; then
      # Fix install to match default volume layout
      unlink lib64 || true
      unlink include || true
      cp -R --remove-destination targets/x86_64-linux/* .
      ln -s lib lib64 || true
      rm -rf targets
      rm include/cuda_dont_kill_me
    fi

    # check if version.txt exists
    ls ${toolkit.dir}/${cuda_versionfile}

  pre-build-hook/unix.command: |
    if ${cuda_fix_install}; then 
      mkdir -p ${toolkit.dir}/include
      touch ${toolkit.dir}/include/cuda_dont_kill_me
      mkdir -p ${toolkit.dir}/bin
      mkdir -p ${toolkit.dir}/lib
    fi

  shell.command: |
    unset DISPLAY
    wget ${cuda_dl_url} -O cuda.run
    chmod +x cuda.run
    ./cuda.run --silent --toolkitpath=${toolkit.dir} --toolkit --override --defaultroot=${toolkit.dir} --no-man-page || true


versions:
- name: '11.7'
  variables:
    cuda_dl_url: https://developer.download.nvidia.com/compute/cuda/11.7.0/local_installers/cuda_11.7.0_515.43.04_linux.run
    cuda_versionfile: version.json

- name: '11.6'
  variables:
    cuda_dl_url: https://developer.download.nvidia.com/compute/cuda/11.6.2/local_installers/cuda_11.6.2_510.47.03_linux.run
    cuda_versionfile: version.json

- name: '11.1'
  variables:
    cuda_dl_url: "https://developer.download.nvidia.com/compute/cuda/11.1.0/local_installers/cuda_11.1.0_455.23.05_linux.run"

- name: '11.2'
  variables:
    cuda_dl_url: "https://developer.download.nvidia.com/compute/cuda/11.2.2/local_installers/cuda_11.2.2_460.32.03_linux.run"
    cuda_versionfile: version.json

- name: '11.0.3'
  variables:
    cuda_dl_url: "https://developer.download.nvidia.com/compute/cuda/11.0.3/local_installers/cuda_11.0.3_450.51.06_linux.run"

- name: '10.2'
  variables:
    pre-build-hook/unix.command: |
      if ${cuda_fix_install}; then 
        mkdir -p ${toolkit.dir}
        # this installer does not work if include dir exists... 
        # thanks nvidia
        mv ${toolkit.dir}/include ${toolkit.dir}/include-bak || true
      fi

    post-build-hook/unix.command: |
      cd ${toolkit.dir}
      if ${cuda_fix_install}; then
        # Fix install to match default volume layout
        unlink lib64 || true
        unlink include || true
        mv include-bak include || true
        cp -R --remove-destination targets/x86_64-linux/* .
        ln -s lib lib64 || true
        rm -rf targets
        rm include-bak || true
      fi

    shell.command: |
      unset DISPLAY
      wget -c https://developer.download.nvidia.com/compute/cuda/10.2/Prod/local_installers/cuda_10.2.89_440.33.01_linux.run -O cuda.run
      wget -c https://developer.download.nvidia.com/compute/cuda/10.2/Prod/patches/1/cuda_10.2.1_linux.run -O p1.run
      wget -c https://developer.download.nvidia.com/compute/cuda/10.2/Prod/patches/2/cuda_10.2.2_linux.run -O p2.run

      chmod +x *.run
      ./cuda.run --silent --toolkitpath=${toolkit.dir} --toolkit --defaultroot=${toolkit.dir} --no-man-page --override
      ./p1.run --silent --toolkitpath=${toolkit.dir} --toolkit --defaultroot=${toolkit.dir} --no-man-page --override
      ./p2.run --silent --toolkitpath=${toolkit.dir} --toolkit --defaultroot=${toolkit.dir} --no-man-page --override

- name: '10.1.105'
  variables:
    cuda_dl_url: "https://developer.nvidia.com/compute/cuda/10.1/Prod/local_installers/cuda_10.1.105_418.39_linux.run"

- name: '10.1'
  variables:
    cuda_dl_url: "http://developer.download.nvidia.com/compute/cuda/10.1/Prod/local_installers/cuda_10.1.243_418.87.00_linux.run"

- name: '10.0'
  variables:
    shell.command: |
      unset DISPLAY
      wget -c https://developer.nvidia.com/compute/cuda/10.0/Prod/local_installers/cuda_10.0.130_410.48_linux -O cuda.run
      wget -c http://developer.download.nvidia.com/compute/cuda/10.0/Prod/patches/1/cuda_10.0.130.1_linux.run -O p1.run

      chmod +x *.run
      ./cuda.run --silent --toolkitpath=${toolkit.dir} --toolkit --override
      ./p1.run --silent --accept-eula --installdir=${toolkit.dir}

    post-build-hook/unix.command: |
      cd ${toolkit.dir}
      if ${cuda_fix_install}; then
        # Fix install to match default volume layout
        unlink lib64 || true
        unlink include || true
        mv include-bak include || true
        ln -s lib lib64 || true
        rm include-bak || true
      fi

- name: '9.2'
  variables:
    cuda_fix_install: false
    cuda_check_error: false
    shell.command: |
      unset DISPLAY
      wget -c https://developer.nvidia.com/compute/cuda/9.2/Prod2/local_installers/cuda_9.2.148_396.37_linux -O cuda.run
      wget -c https://developer.nvidia.com/compute/cuda/9.2/Prod2/patches/1/cuda_9.2.148.1_linux -O p1.run

      chmod +x *.run
      ./cuda.run --silent --toolkitpath=${toolkit.dir} --toolkit --override
      ./p1.run --silent --accept-eula --installdir=${toolkit.dir}

- name: '9.1'
  variables:
    cuda_fix_install: false
    cuda_check_error: false
    shell.command: |
      unset DISPLAY
      wget -c https://developer.nvidia.com/compute/cuda/9.1/Prod/local_installers/cuda_9.1.85_387.26_linux -O cuda.run
      wget -c https://developer.nvidia.com/compute/cuda/9.1/Prod/patches/1/cuda_9.1.85.1_linux -O p1.run
      wget -c https://developer.nvidia.com/compute/cuda/9.1/Prod/patches/2/cuda_9.1.85.2_linux -O p2.run
      wget -c https://developer.nvidia.com/compute/cuda/9.1/Prod/patches/3/cuda_9.1.85.3_linux -O p3.run

      chmod +x *.run
      ./cuda.run --silent --toolkitpath=${toolkit.dir} --toolkit --override

      ./p1.run --silent --accept-eula --installdir=${toolkit.dir}
      ./p2.run --silent --accept-eula --installdir=${toolkit.dir}
      ./p3.run --silent --accept-eula --installdir=${toolkit.dir}

- name: '9.0'
  variables:
    cuda_fix_install: false
    cuda_check_error: false
    shell.command: |
      unset DISPLAY
      wget -c https://developer.nvidia.com/compute/cuda/9.0/Prod/local_installers/cuda_9.0.176_384.81_linux-run -O cuda.run

      sh ./cuda.run --silent --toolkitpath=${toolkit.dir} --toolkit --override
      for i in 1 2 3 4; do
        wget -c https://developer.nvidia.com/compute/cuda/9.0/Prod/patches/\$i/cuda_9.0.176.\${i}_linux-run -O p\$i.run
        sh ./p\$i.run --silent --accept-eula --installdir=${toolkit.dir}
      done

- name: '8.0'
  variables:
    cuda_fix_install: false
    cuda_check_error: false
    shell.command: |
      unset DISPLAY
      wget -c https://developer.nvidia.com/compute/cuda/8.0/Prod2/local_installers/cuda_8.0.61_375.26_linux-run -O cuda.run
      chmod +x cuda.run
      ./cuda.run --silent --toolkitpath=${toolkit.dir} --toolkit --override
      wget -c https://developer.nvidia.com/compute/cuda/8.0/Prod2/patches/2/cuda_8.0.61.2_linux-run -O cuda_patch2.run
      chmod +x cuda_patch2.run
      ./cuda_patch2.run --silent --accept-eula --installdir=${toolkit.dir}

- name: '7.5'
  variables:
    cuda_fix_install: false
    cuda_check_error: false
    shell.command: |
      unset DISPLAY
      wget -c http://developer.download.nvidia.com/compute/cuda/7.5/Prod/local_installers/cuda_7.5.18_linux.run -O cuda.run
      chmod +x cuda.run
      ./cuda.run --silent --toolkitpath=${toolkit.dir} --toolkit --override

- name: '6.5'
  variables:
    cuda_fix_install: false
    cuda_check_error: false
    shell.command: |
      unset DISPLAY
      wget -c http://developer.download.nvidia.com/compute/cuda/6_5/rel/installers/cuda_6.5.14_linux_64.run -O cuda.run
      chmod +x cuda.run
      ./cuda.run --silent --extract=/tmp/cuda_tmp
      /tmp/cuda_tmp/cuda-linux64-rel-6.5.14-18749181.run -prefix=${toolkit.dir} -noprompt
      /tmp/cuda_tmp/cuda-samples-linux-6.5.14-18745345.run -noprompt -prefix=${toolkit.dir}/share/cuda-6.5/ -cudaprefix=${toolkit.dir}
      cd ${toolkit.dir}/share/cuda-6.5/0_Simple/clock
      make
      rm -rf /tmp/cuda_tmp
