catalog:
  title: Tiago Simulation
  version: melodic

variables:
  description: |
    Minimal distribution for Tiago Sim on 18.04 and ROS Melodic.

    Includes vdemo and tobi sim files
  recipe.maintainer:
  - Leroy Rügemer <lruegeme@techfak.uni-bielefeld.de>
  
  toolkit.volume:    /vol/tiago/melodic
  toolkit.dir:       ${toolkit.volume}/sim

  ros: melodic
  access: public

  cudagen: All

  shell.environment:${mode}:
  - PKG_CONFIG_PATH="${toolkit.dir}/lib/pkgconfig:\${PKG_CONFIG_PATH}"
  - CMAKE_PREFIX_PATH="${toolkit.dir}:\${CMAKE_PREFIX_PATH}"
  - ROS_MAVEN_REPOSITORY=file://${toolkit.dir}/share/repository/
  - ROS_MAVEN_DEPLOYMENT_REPOSITORY=${toolkit.dir}/share/repository/

  message: |
    you need ubuntu ros repositories to install dependencies, run the following commands to install them:

      sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
      sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key C1CF6E31E6BADE8868B172B4F42ED6FBAB17C654
      sudo apt update
      sudo apt install ros-melodic-ros-base
      sudo rosdep init
      rosdep update

  platform-requires:
    ubuntu:
      packages: [ros-melodic-desktop-full, ros-melodic-moveit-simple-controller-manager, python-catkin-tools ros-melodic-joint-state-controller, ros-melodic-twist-mux, ros-melodic-ompl, ros-melodic-controller-manager, ros-melodic-moveit-core, ros-melodic-moveit-ros-perception, ros-melodic-moveit-ros-move-group, ros-melodic-moveit-kinematics, ros-melodic-moveit-ros-planning-interface, ros-melodic-moveit-simple-controller-manager, ros-melodic-moveit-planners-ompl, ros-melodic-joy, ros-melodic-joy-teleop, ros-melodic-teleop-tools, ros-melodic-control-toolbox, ros-melodic-sound-play, ros-melodic-navigation, ros-melodic-depthimage-to-laserscan, ros-melodic-moveit-commander]

# Erbium installed PKGs (from kinetic)
# autoconf, automake, bison, buffer, cmake, doxygen, doxygen, graphviz, imagemagick, inotify-tools, iwidgets4, libasound2-dev, libatlas-base-dev, libavcodec-dev, libavdevice-dev, libavformat-dev, libavresample-dev, libavresample-dev, libavutil-dev, libboost-all-dev, libboost-iostreams-dev, libcuda1-384, libeigen3-dev, libespeak-dev, libflann-dev, libfreenect-dev, libgflags-dev, libglew-dev, libglew-dev, libgoogle-glog-dev, libgphoto2-dev, libgraphicsmagick++1-dev, libgraphicsmagick1-dev, libhdf5-serial-dev, libjpeg8-dev, libjpeg-dev, libleveldb-dev, liblmdb-dev, libmagick++-dev, libmagick++-dev, libmagick++-dev, libnetpbm10-dev, libopenblas-dev, libopencv-calib3d-dev, libopencv-core-dev, libopencv-dev, libopencv-highgui-dev, libopencv-imgproc-dev, libopencv-video-dev, libpng-dev, libprotobuf-dev, libprotobuf-dev, libqt5opengl5-dev, libqt5svg5-dev, libqt5webkit5-dev, libsnappy-dev, libswscale-dev, libtbb2, libtbb-dev, libudev-dev, libusb-dev, libxine2-dev, lvtk-dev, openssh-client, pal, protobuf-compiler, protobuf-compiler, python-catkin-tools, python-coverage, python-mock, python-numpy, python-pip, python-pyassimp, python-pyparsing, python-sphinx, python-sphinx, python-statsd, qtbase5-dev, qtbase5-dev-tools, qtmultimedia5-dev, ros-melodic-controller-manager, ros-melodic-cv-bridge, ros-melodic-depthimage-to-laserscan, ros-melodic-desktop-full,  ros-melodic-four-wheel-steering-msgs, ros-melodic-gmapping, ros-melodic-grasping-msgs,  ros-melodic-joint-trajectory-controller, ros-melodic-joy, ros-melodic-joy-teleop, ros-melodic-moveit-core, ros-melodic-moveit-resources, ros-melodic-moveit-ros-move-group, ros-melodic-moveit-ros-planning, ros-melodic-moveit-ros-planning-interface, ros-melodic-navigation, ros-melodic-ompl, ros-melodic-openni-launch, ros-melodic-people-msgs, ros-melodic-pointcloud-to-laserscan, ros-melodic-position-controllers, ros-melodic-realtime-tools, ros-melodic-sick-tim, ros-melodic-twist-mux, ros-melodic-urdf-geometry-parser, ros-melodic-uuid-msgs, ros-melodic-vision-msgs, ros-melodic-visualization-msgs, sloccount, swig, tclx8.4, unp, python-bson, ros-melodic-openni2-launch, ros-melodic-usb-cam, libqwt-qt5-dev, libqwt-dev, ros-melodic-trac-ik-kinematics-plugin, ros-melodic-moveit-ros-visualization, ros-melodic-moveit-planners-ompl, ros-melodic-diff-drive-controller, ros-melodic-joint-state-controller, ros-melodic-spatio-temporal-voxel-layer

# Missing in melodic 
# ros-melodic-aruco-ros, ros-melodic-eband-local-planner, ros-melodic-humanoid-nav-msgs,

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    rm -rf "${toolkit.dir}"/*
    for d in include lib/python2.7/dist-packages bin share; do mkdir -p "${toolkit.dir}/\$d"; done
    ln -s ${toolkit.dir}/lib ${toolkit.dir}/lib64
    ln -s ${toolkit.dir}/lib/python2.7/dist-packages ${toolkit.dir}/lib/python2.7/site-packages

include:
- stacks/tiago/sim9
- stacks/tiago/system-startup

versions:

# Clf startup stuff
- ros-tobi_sim@master
- ros-pose_publisher@master


