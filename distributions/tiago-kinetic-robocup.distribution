catalog:
  title: Tiago Software
  version: kinetic-robocup

variables:
  description: Tiago Kinetic Sim with robocup software
  recipe.maintainer:
  - Leroy Rügemer <lruegeme@techfak.uni-bielefeld.de>
  - David Leins <dleins@techfak.uni-bielefeld.de>
  
  toolkit.volume:    /tmp
  toolkit.dir:       ${toolkit.volume}/tiago-sim

  ros: kinetic
  access: private

  restrict-to-slaves: "erbium"

  cudagen: All

  shell.environment:${mode}:
  - PKG_CONFIG_PATH="${toolkit.dir}/lib/pkgconfig:\${PKG_CONFIG_PATH}"
  - CMAKE_PREFIX_PATH="${toolkit.dir}:\${CMAKE_PREFIX_PATH}"
  - ROS_MAVEN_REPOSITORY=file://${toolkit.dir}/share/repository/
  - ROS_MAVEN_DEPLOYMENT_REPOSITORY=${toolkit.dir}/share/repository/

  platform-requires:
    ubuntu:
      packages: [autoconf, automake, bison, buffer, cmake, doxygen, doxygen, graphviz, imagemagick, inotify-tools, iwidgets4, libasound2-dev, libatlas-base-dev, libavcodec-dev, libavdevice-dev, libavformat-dev, libavresample-dev, libavresample-dev, libavutil-dev, libboost-all-dev, libboost-iostreams-dev, libcuda1-384, libeigen3-dev, libespeak-dev, libflann-dev, libfreenect-dev, libgflags-dev, libglew-dev, libglew-dev, libgoogle-glog-dev, libgphoto2-dev, libgraphicsmagick++1-dev, libgraphicsmagick1-dev, libhdf5-serial-dev, libjpeg8-dev, libjpeg-dev, libleveldb-dev, liblmdb-dev, libmagick++-dev, libmagick++-dev, libmagick++-dev, libnetpbm10-dev, libopenblas-dev, libopencv-calib3d-dev, libopencv-core-dev, libopencv-dev, libopencv-highgui-dev, libopencv-imgproc-dev, libopencv-video-dev, libpng12-dev, libprotobuf-dev, libprotobuf-dev, libqt5opengl5-dev, libqt5svg5-dev, libqt5webkit5-dev, libsnappy-dev, libswscale-dev, libtbb2, libtbb-dev, libudev-dev, libusb-dev, libxine2-dev, lvtk-dev, maven, nvidia-384-dev, nvidia-opencl-dev, ocl-icd-libopencl1, opencl-headers, opencl-headers, openjdk-8-jdk, openjdk-8-jre, openjfx, openssh-client, pal, protobuf-compiler, protobuf-compiler, python-catkin-tools, python-coverage, python-mock, python-numpy, python-pip, python-pyassimp, python-pyparsing, python-sphinx, python-sphinx, python-statsd, qtbase5-dev, qtbase5-dev-tools, qtmultimedia5-dev, ros-kinetic-controller-manager, ros-kinetic-cv-bridge, ros-kinetic-depthimage-to-laserscan, ros-kinetic-desktop-full, ros-kinetic-eband-local-planner, ros-kinetic-four-wheel-steering-msgs, ros-kinetic-gmapping, ros-kinetic-grasping-msgs, ros-kinetic-humanoid-nav-msgs, ros-kinetic-joint-trajectory-controller, ros-kinetic-joy, ros-kinetic-joy-teleop, ros-kinetic-moveit-core, ros-kinetic-moveit-resources, ros-kinetic-moveit-ros-move-group, ros-kinetic-moveit-ros-planning, ros-kinetic-moveit-ros-planning-interface, ros-kinetic-navigation, ros-kinetic-ompl, ros-kinetic-openni-launch, ros-kinetic-people-msgs, ros-kinetic-pointcloud-to-laserscan, ros-kinetic-position-controllers, ros-kinetic-realtime-tools, ros-kinetic-sick-tim, ros-kinetic-twist-mux, ros-kinetic-urdf-geometry-parser, ros-kinetic-uuid-msgs, ros-kinetic-vision-msgs, ros-kinetic-visualization-msgs, screen, sloccount, swig, tclx8.4, tk, unp, python-bson, ros-kinetic-openni2-launch, ros-kinetic-usb-cam, libqwt-qt5-dev, libqwt-dev, ros-kinetic-trac-ik-kinematics-plugin, ros-kinetic-aruco-ros, ros-kinetic-moveit-ros-visualization, ros-kinetic-moveit-planners-ompl, ros-kinetic-diff-drive-controller, ros-kinetic-joint-state-controller, ros-kinetic-spatio-temporal-voxel-layer]

include:
- stacks/tiago/deps-xenial
- stacks/tiago/sim8
- stacks/robocup/tiago-software
- stacks/tiago/grasping
- stacks/tiago/objrec

versions:

- maven-repo-cleanup@master




