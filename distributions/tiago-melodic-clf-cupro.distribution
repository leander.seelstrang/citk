catalog:
  title: Tiago Software
  version: melodic-clf-cupro

variables:
  description: Clean Up robot software ontop of Tiago Melodic Dist
  recipe.maintainer:
    - Leroy Rügemer <lruegeme@techfak.uni-bielefeld.de>

  toolkit.volume:    /vol/tiago/melodic
  toolkit.dir:       ${toolkit.volume}/cupro

  ros.underlay: ${toolkit.volume}/clf
  ros: melodic
  access: private

  java.jdk: Java11

  shell.environment:${mode}:
    - CXXFLAGS="-march=native -o2"

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    for d in include lib/python2.7/dist-packages bin share; do mkdir -p "${toolkit.dir}/\$d"; done
    ln -s ${toolkit.dir}/lib ${toolkit.dir}/lib64
    ln -s ${toolkit.dir}/lib/python2.7/dist-packages ${toolkit.dir}/lib/python2.7/site-packages

versions:

- name: deploy-volume
  version: master
  parameters:
    user: pal
    group: pal
    remote-host: tiago-47c

#######################################
## Deps
#######################################

## ECWM

- name: gcc
  version: "9.3.0"
  parameters:
    timeout/minutes: 600
    shell.environment:${mode}: 

#- geometric_shapes@noetic-devel

## groceries_pick
- name: pcl
  version: pcl-1.11.1
  parameters:
    timeout/minutes: 600

- ros-perception_pcl@melodic-devel

## Viz Stuff, rebuild rviz
#- rviz@melodic-devel
# Displays
- ros-clf_object_recognition_msgs@melodic-devel #dep
- ros-clf_object_recognition_rviz@melodic-devel

- moveit@tiago
- moveit_task_constructor@master

#######################################
## Pkgs
#######################################

- ros-groceries_pick_server@master

#######################################
## Private Pkgs
#######################################

- cupro_sim@master

- name: ecwm
  version: master
  parameters:
    cmake.options: 
      # Ok, two pkgs require gcc9, both have the same check inside CMakeLists
      # 'ecwm_core' is building first and is successfull
      # Then, 'ecwm_plugins' is build and it uses system gcc(7)?
      # We build them both together using catkin build..
      # Bad catkin, bad!
      # But, we can force cmake to use the installed version with this
      - CMAKE_CXX_COMPILER="${toolkit.dir}/bin/gcc"
      - CMAKE_CXX_IMPLICIT_INCLUDE_DIRECTORIES="${toolkit.dir}/include/c++/9.3.0/"
      - CMAKE_CXX_STANDARD_LIBRARIES="${toolkit.dir}/lib64/libstdc++.so"
      - DISABLE_CLANG_TIDY=ON
      - '@{next-value|[]}'