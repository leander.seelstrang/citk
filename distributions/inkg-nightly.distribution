catalog:
  title: INKG
  version: nightly

variables:
  filesystem.group/unix: csra

  prepare-hook/unix: |
    mkdir -p "${toolkit.dir}"
    chmod 2755 "${toolkit.dir}"
    mkdir -p "${toolkit.dir}/var"
    chmod 2775 "${toolkit.dir}/var"
    chgrp -R csra "${toolkit.dir}"

  finish-hook/unix: |
    find "${toolkit.dir}" -exec chgrp ${filesystem.group/unix} {} \;
    find "${toolkit.dir}" -type d -exec chmod 2755 {} \;
    find "${toolkit.dir}" -type f -exec chmod go+r {} \;
    find "${toolkit.dir}" -type f -perm /100 -exec chmod go+x {} \;
    find "${toolkit.dir}/var" -type d -exec chmod 2775 {} \;
    find "${toolkit.dir}/var" -type f -exec chmod go+rw {} \;
    find "${toolkit.dir}/var" -type f -perm /100 -exec chmod go+x {} \;
    chmod -R g+x "${toolkit.dir}/bin"
    chgrp -R csra "${toolkit.dir}/var"

  slaves:
  - master
  slaves.linux:
  - '@{slaves.ubuntu}'
  slaves.macos:
  - MacOS_Lion_64bit
  slaves.ubuntu:
  - '@{slaves}'
  slaves.windows:
  - Windows_7_32bit
  - Windows_7_64bit

  toolkit.dir: ${toolkit.volume}/releases/trusty/${distribution-name}
  toolkit.volume: /vol/csra/
  variant: nightly
  access: private

versions:
- spread                                                    @4.3
- vdemo                                                     @master
- lsp-csra-system-startup                                   @master
- rsc                                                       @0.11
- rsb-protocol                                              @0.11
- rsb-cpp                                                   @0.11
- rsb-python                                                @0.11
- rsb-tools-cpp                                             @0.11
- rsb-spread-cpp                                            @0.11
- rst-proto-csra                                            @master
- rst-converters-cpp                                        @0.11
- rst-converters-python                                     @0.11
- rsb-xml-cpp                                               @0.11
- rsb-xml-python                                            @0.11
- libinkg                                                   @master
- inkg-rsb-interface                                        @master
