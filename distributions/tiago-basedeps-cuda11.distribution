catalog:
  title: Tiago Basedeps
  version: cuda11.0
  description: |
    Base Dependencies for Tiago Distributions
    Contains long running jobs
    This should be build once and then copied into the target distribution

variables:
  recipe.maintainer:
  - lruegeme@techfak.uni-bielefeld.de
  access: private

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    mkdir -p "${toolkit.dir}" ; chmod 2755 "${toolkit.dir}"

  finish-hook-name: distribution-finish-${distribution-name}
  finish-hook/unix: |
    chmod -R g+x ${toolkit.dir}/bin

    # cleanup ros pkgs
    cd ${toolkit.dir}
    rm _setup_util.py env.sh local_setup.bash local_setup.sh local_setup.zsh setup.bash setup.sh setup.zsh 
    rm -rf .catkin

  ros: ${next-value|melodic}

  toolkit.volume: /vol/tiago/${ros}
  toolkit.dir: ${toolkit.volume}/cuda11.0

versions:

- cmake@3.18.2

- name: gcc
  version: "9.3.0"
  parameters:
    timeout/minutes: 600
    shell.environment:${mode}: 

- cuda@11.0.3

- name: cudnn
  version: "8.0"
  parameters:
    cuda-version: '11.0'

- name: caffe
  version: cudnn8
  parameters:
    cmake.options:
      - '@{next-value|[]}'
      - CUDA_ARCH_NAME=Manual
      - CUDA_ARCH_BIN="60 61 62"
      - CUDA_ARCH_PTX=""

- name: opencv-plus-contrib-cuda
  version: cuda-10
  parameters:
    timeout/minutes: 900

# Reinstall with CUDA support
- name: pcl
  version: pcl-1.11.1
  parameters:
    timeout/minutes: 600
    make.threads: 2

# Reinstall with updated dependencies

## Deps on libopencv-dev
- ros-vision_opencv@melodic # image_geometry cv_bridge
- ros-image_transport_plugins@melodic # theora_image_transport compressed_image_transport compressed_depth_image_transport
- ros-image_pipeline@melodic # depth_image_proc image_publisher image_rotate stereo_image_proc image_view image_proc
- ros-rgbd_launch@melodic
- ros-rqt_image_view@0.4.16

## Deps on libpcl-all-dev:
- ros-perception_pcl@melodic-devel