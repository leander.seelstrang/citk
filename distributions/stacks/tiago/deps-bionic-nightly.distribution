variables:
  description: Tiago software stack dependencies for nightly build
  recipe.maintainer:
  - Leroy Rügemer <lruegeme@techfak.uni-bielefeld.de>
  - David Leins <dleins@techfak.uni-bielefeld.de>

  access: public

versions:

- cmake@3.18.2

- cuda@11.0.3

- name: cudnn
  version: "8.0"
  parameters:
    cuda-version: '11.0'

- name: caffe
  version: tiago-clf-cv4
  parameters:
    patches.diff-strings:
      - !b!literal-include ../../../projects/patches/caffe_cudnn8.patch

- name: opencv-plus-contrib-cuda
  version: cuda-10
  parameters:
    timeout/minutes: 900

# Reinstall with CUDA support
- name: pcl
  version: pcl-1.11.1
  parameters:
    timeout/minutes: 600
    make.threads: 4

- tensorflow@1.14.0
- tensorflow-object-detection-api@master

- name: cmu-openpose
  version: v1.6.0
  parameters:
    patches.diff-strings:
      - !b!literal-include ../../../projects/patches/openpose_cudnn8.diff
      - !b!literal-include ../../../projects/patches/cmu_openpose1.6.patch
    cmake.options:
      - "BUILD_CAFFE=off"
      - "CMAKE_INSTALL_PREFIX=${toolkit.dir}"

- ros-people_tracking_filter@melodic
# - ros-people_tracking_filter@clf-melodic

# from deps-xenial-test
- ros-vision_opencv@tiago-clf

