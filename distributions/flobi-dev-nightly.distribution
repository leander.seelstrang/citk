catalog:
  title: Flobi Development
  version: nightly

variables:
  access: private

  platform-requires:
    precise:
      packages:
      - libstdc++6-4.6-dev
      - libhighgui2.3
      - libcv2.3
      - libopencv-gpu2.3
      - libcvaux-dev
      - libcv-dev
      - libhighgui-dev
      - libopencv-gpu-dev
      - ros-hydro-ros-base
    trusty:
      packages:
      - libstdc++6-4.7-dev
      - libhighgui2.4
      - libcv2.4
      - libopencv-gpu2.4
      - libcvaux-dev
      - libcv-dev
      - libhighgui-dev
      - libopencv-gpu-dev
      - ros-indigo-ros-base
    ubuntu:
      packages:
      - blender
      - cmake
      - build-essential
      - python3
      - python3-dev
      - python3-setuptools
      - python3-yaml
      - g++-multilib
      - libc6-dev-i386
      - libgstreamer0.10-dev
      - libgstreamer-plugins-base0.10-dev
      - gstreamer0.10-alsa
      - gstreamer0.10-plugins-base
      - gstreamer0.10-plugins-good
      - gstreamer0.10-tools
      - gstreamer0.10-pulseaudio
      - libc6-dev-i386
      - libao-dev

  toolkit.dir: ${toolkit.volume}/${distribution-name}
  toolkit.volume: /vol/flobi/releases/

  filesystem.group/unix: clf

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    mkdir -p "${toolkit.dir}"
    chmod 2755 "${toolkit.dir}"

  finish-hook-name: distribution-finish-${distribution-name}
  finish-hook/unix: |
    find "${toolkit.dir}" -exec chgrp ${filesystem.group/unix} {} \;
    find "${toolkit.dir}" -type d -exec chmod 2755 {} \;
    find "${toolkit.dir}" -type f -exec chmod go+r {} \;
    find "${toolkit.dir}" -type f -perm /100 -exec chmod go+x {} \;
    chmod -R g+x "${toolkit.dir}"/bin

versions:
- spread                                                    @4.4
- vdemo                                                     @master
- libreflexxes                                              @1.2.6
- flobidev-core-humotion                                    @master
- flobidev-core-client_cpp                                  @master
- flobidev-core-control                                     @master
- flobidev-core-tools                                       @master
- flobidev-core-server                                      @master
- flobidev-core-sim                                         @master
- flobidev-basic-hlc_server_rsb                             @master
- flobidev-basic-hlc_client_java                            @master
- rsc                                                       @0.11
- rsb-protocol                                              @0.11
- rsb-cpp                                                   @0.11
- rsb-java                                                  @0.11
- rsb-python                                                @0.11
- rsb-tools-cpp                                             @0.11
- rsb-spread-cpp                                            @0.11
- rsb-tools-cl-binary                                       @0.11
- rsbag-tools-cl-binary                                     @0.11
- rst-proto-csra                                            @master
- rst-converters-cpp                                        @0.11
- rst-converters-python                                     @0.11
- rsb-gstreamer                                             @0.11
- rsb-opencv                                                @0.11
- rsb-video-writer                                          @master
- python3-rospkg                                            @1.0.20
- python3-catkin-pkg                                        @0.1.10
- name: ros-catkin
  version: 0.5.65
  parameters:
    python.version: '3'
- morse-tarball                                             @1.2.1
- pyscxml                                                   @v.0.8.4-fsmt
- fsmt                                                      @0.17
