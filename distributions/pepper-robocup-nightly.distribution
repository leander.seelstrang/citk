catalog:
  title: Pepper SSPL RoboCup
  version: nightly

variables:
  access: private
  recipe.maintainer:
  - flier@techfak.uni-bielefeld.de

  ros: kinetic

  toolkit.volume: /vol/pepper/systems
  toolkit.dir: ${toolkit.volume}/${distribution-name}

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    mkdir -p "${toolkit.dir}/opt";
    chmod 2755 "${toolkit.dir}"

  finish-hook-name: distribution-finish-${distribution-name}
  finish-hook/unix: |
    find "${toolkit.dir}" -type d -exec chmod 2755 {} \;
    find "${toolkit.dir}" -type f -exec chmod go+r {} \;
    find "${toolkit.dir}" -type f -perm /100 -exec chmod go+x {} \;
    chmod -R g+x "${toolkit.dir}"/bin

versions:
- aldebaran-naoqi-sdk-python27                              @2.5.5.5
- aldebaran-naoqi-sdk-cpp                                   @2.5.5.5
- aldebaran-choregraphe-internal                            @2.5.5.5
- fsmt                                                      @0.5
- fsmt-exp-pepper-robocup                                   @master
- pyscxml                                                   @v.0.8.5-fsmt
- python3-rospkg                                            @1.0.37
- python3-catkin-pkg                                        @0.2.10
- pepper-vdemo-config                                       @master
- hri-morse                                                 @master
- pepper-morse-data                                         @master
- pepper-morse-simcontroller                                @master
- pepper-morse-ros-bridge                                   @master
- pepper-docs                                               @master
- ros-navigation_msgs                                       @clf
- ros-navigation                                            @clf
- ros-keyboard_teleop                                  @llach
- pepper-bringup                                            @master
- pepper-description                                        @urdf_master_merge
- pepper-nav-bringup                                        @post_madgeburg_tuning
- pepper-ros-image-viz                                      @master
- pepper-nav-maps                                           @master
- pepper-hwreport                                           @master
- pepper-tabletservice                                      @speechrec_dev
- pepper-sensors-py                                         @master
- pepper-actuators-py                                       @master
- pepper-clf-msgs                                           @master
- pepper-perception-configs                                 @master
- pepper-naoqi-driver-py                                    @pepper-robocup
- pepper-naoqi-driver-cpp                                   @no_moveTo_call
- pcl                                                       @pcl-1.8.0
- cuda                                                      @8.0
- cudnn                                                     @6.0
- opencv-plus-contrib-cuda                                 @3.3.1
- ros-clf-perception-vision-msgs                            @master
- clf_perception_vision                                     @master
- cftldROS_tracking                                         @pepper-experimental
- vdemo                                                     @robocup-experimental-noxterm
- dlib                                                      @v19.7
- dlib-python                                               @v19.7-cuda
- dataset-dlib                                              @dlib.net
- cnn_age_and_gender_model                                  @openu.ac.il
- naoqi_bridge_msgs                                         @pepper-robocup
- pepper-naoqi-pose-py                                      @stable-hash
- openpose_ros                                              @pepper_dev
- cmu-openpose                                              @v1.3.0
- caffe                                                     @pepper-robocup
- robocup-openpose-models                                   @master
- tensorflow                                                @1.4.0
- ros-object-recognition-tensorflow-ros                     @detection
- ros-object-recognition-tensorflow-ros-rqt                 @detection
- ros-object-tracking-msgs                                  @master
- ros-object-recognition-image-recognition-rqt              @detection
- ros-object-recognition-image-recognition-util             @detection
- ros-object-recognition-image-recognition-externals        @pepper-robocup-2018
- darknet_ros_ethz                                          @pepper_robocup
- robocup-robosync                                          @master
- ros-ira-laser-tools                                       @pre_magdeburg
- ros-laser_filters                                         @kinetic-devel
- bayestracking                                             @clf_ptracker_extended
- bayes_people_tracker                                      @clf_ptracker_extended
- ros-filters                                               @lunar-devel
- ageitgey-face-recognition                                 @pepper-robocup
- pepper-face-id-ros                                        @uuid-devel
- dataset-keras-gender_age                                  @18-4.06
- dataset-tensorflow-objects                                @1.0
- python-mqtt                                               @1.3.1
- rospy-message-converter                                   @0.4.0
- cmusphinx-sphinxbase                                      @master
- cmusphinx-pocketsphinx                                    @master
- pocketsphinx-adapter-catkin-ros                           @no_rsb_remote_gui
- pocketsphinx-adapter-configs                              @projects
- pocketsphinx-adapter-grammars                             @projects
- rosjava_build_tools                                       @clf
- rosjava_bootstrap                                         @clf
- name: rosjava_genjava
  versions:
  - version: clf
  - version: create_messages
- rosjava_test_msgs                                         @kinetic
- rosjava_core                                              @clf
- rosjava_actionlib                                         @kinetic
- bonsai2-core                                              @master
- bonsai2-interface                                         @master
- bonsai2-skill                                             @master
- bonsai2-adapter                                           @norsb
- bonsai2-scxml                                             @norsb
- bonsai2-robocup-dist                                      @no_bin
- bonsai2-pepper-dist                                       @master
- statemachine_visualisation                                @master
- ros-bonsai_msgs                                           @master
- ros-augmented-manipulation-msgs                           @master
- ros-biron_posture_execution-msgs                          @posture_execution
- ros-clf_posture_execution_msgs                            @master
- openpose_ros_msgs                                         @pepper_dev
- bayes_people_tracker_msgs                                 @clf_ptracker_extended
- ros-openni2-tracker-msgs                                  @master
- ros-planning-scene-manager-msgs                           @master
- face_gender_age_detection_msgs                            @master
- ros-hand_over_msgs                                        @master
- ros-hand_shaker_msgs                                      @master
- ros-force_guiding_msgs                                    @master
- ros-tobi_world-msgs                                       @master
- knowledge_base_msgs                                       @master
- ros-range_sensor_layer                                    @indigo
- xmltio                                                    @tar4275
- btl-cpp                                                   @1.1
- knowledge_base                                            @robocup
- annotate                                                  @rsb-less
- pepper-grasp-ml-ros                                       @master
- tfmodellib                                                @master
- moveit                                                    @agni-kinetic-devel
- pepper-moveit-config                                      @master
- moveit_task_constructor                                   @llach
- pepper-mtc                                                @hololens_dev
- maven-repo-cleanup                                        @master
- icl-github                                                @pepper-grasping
- icl_ros_segmentation                                      @pepper-devel
- iclros_bridge                                             @master
- agni_robot_viz                                            @pepper-devel
- ros-grasping-msgs                                         @augmented_pickplace
- agni_grasp_generator                                      @augmented_pickplace
- agni_grasp_manager                                        @augmented_pickplace
- agni_manip_utils                                          @master
- object_fitter                                             @augmented_pickplace
- sq_fitting                                                @master
- sq_fitting_ros                                            @master
- grasp_visualization                                       @augmented_pickplace
- ros-image-transport-toggle                                @hydro-devel
- pepper-image-sink                                         @master
- ros-force_guiding                                         @master
- ros-tf-pose-estimation                                    @master
- rosbridge-server                                          @0.8.6
- rosapi                                                    @0.8.6
- rosbridge-library                                         @0.8.6
- ros-auth                                                  @develop
- clf_people_attribute_server                               @master
- ros-slam_gmapping                                              @hydro-devel
