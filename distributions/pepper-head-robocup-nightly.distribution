catalog:
  title: Pepper Head SSPL RoboCup
  version: nightly

variables:
  access: private
  recipe.maintainer:
  - flier@techfak.uni-bielefeld.de
  ros: kinetic

  toolkit.volume: /vol/pepper/systems/
  toolkit.dir: ${toolkit.volume}/pepper-robocup-nightly

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    mkdir -p "${toolkit.dir}/opt";
    chmod 2755 "${toolkit.dir}"

  finish-hook-name: distribution-finish-${distribution-name}
  finish-hook/unix: |
    find "${toolkit.dir}" -type d -exec chmod 2755 {} \;
    find "${toolkit.dir}" -type f -exec chmod go+r {} \;
    find "${toolkit.dir}" -type f -perm /100 -exec chmod go+x {} \;
    chmod -R g+x "${toolkit.dir}"/bin

versions:
- pepper-dcm-bringup                                        @master
- pepper-bringup                                            @master
- pepper-naoqi-driver-py                                    @pepper-robocup
- pepper-naoqi-driver-cpp                                   @move_controller
- pepper-sensors-py                                         @master
- pepper-actuators-py                                       @master
- pepper-clf-msgs                                           @master
- pepper-naoqi-pose-py                                      @stable-hash
- pepper-description                                        @urdf_master_merge
- pepper-nav-bringup                                        @post_madgeburg_tuning
- pepper-nav-maps                                           @master
- ros-ira-laser-tools                                       @pre_magdeburg
- ros-filters                                               @lunar-devel
- ros-laser_filters                                         @kinetic-devel
- ros-navigation                                            @clf
- ros-navigation_msgs                                       @clf
- cftldROS_tracking                                         @pepper-experimental
- pepper-hwreport                                           @master
- pepper-naoqi-dcm-driver                                   @master
- pepper-gscam                                              @dev
- cmusphinx-sphinxbase                                      @master
- cmusphinx-pocketsphinx                                    @master
- pocketsphinx-adapter-catkin-ros                           @no_rsb_remote_gui
- pocketsphinx-adapter-configs                              @projects
- pocketsphinx-adapter-grammars                             @projects
- ros-range_sensor_layer                                    @indigo
- bayestracking                                             @master
- bayes_people_tracker                                      @master
- bayes_people_tracker_msgs                                 @clf_ptracker_extended
- control_msgs                                              @kinetic-devel
- ros-sick-tim                                              @kinetic
- ros-usb-cam                                               @develop
- pepper-libmotion-controller                               @2.5
- pepper-vdemo-config                                       @master
- vdemo                                                     @robocup-experimental-noxterm
- ros-tf-pose-estimation                                    @master
- rosbridge-server                                          @0.8.6
- rosapi                                                    @0.8.6
- rosbridge-library                                         @0.8.6
- ros-auth                                                  @develop
- pepper-tabletservice                                      @speechrec_dev
- clf_people_attribute_server                               @master
