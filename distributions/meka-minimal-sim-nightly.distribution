catalog:
  title: meka minimal grasp simulation
  version: nightly

variables:
  recipe.maintainer:
  - plueckin@techfak.uni-bielefeld.de
  - flier@techfak.uni-bielefeld.de

  access: private
  ros: kinetic

  toolkit.volume: /vol/meka
  toolkit.dir: ${toolkit.volume}/simulation/nightly

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    mkdir -p "${toolkit.dir}"
    chmod 2755 "${toolkit.dir}"

  finish-hook-name: distribution-finish-${distribution-name}
  finish-hook/unix: |
    find "${toolkit.dir}" -type d -exec chmod 2755 {} \;
    find "${toolkit.dir}" -type f -exec chmod go+r {} \;
    find "${toolkit.dir}" -type f -perm /100 -exec chmod go+x {} \;

  java.jdk: Java8
  shell.environment:
  - '@{next-value|[]}'

versions:
- spread                                                    @4.4
- spread-python                                             @1.5spread4
- sbcl-binary                                               @1.3.0
- quicklisp                                                 @current
- cl-launch                                                 @master
- cl-architecture.service-provider                          @master
- cl-architecture.builder-protocol                          @master
- cl-network.spread                                         @master
- cl-protobuf                                               @master
- rsc                                                       @0.15
- rsb-protocol                                              @0.15
- rsb-cpp                                                   @0.15
- rsb-spread-cpp                                            @0.15
- rsb-python                                                @0.15
- rsb-cl                                                    @master
- rsb-tools-cpp                                             @0.15
- rsb-tools-cl                                              @feature-norman-bridge
- rsbag-tools-cl-binary                                     @0.15
- rst-proto                                                 @0.15
- rst-converters-cpp                                        @0.15
- rst-converters-python                                     @0.15
- rct-tools                                                 @new_bridge
- rct-cpp                                                   @rsb_collections
- rct-java                                                  @master
- agni_grasp_generator                                      @master
- agni_grasp_manager                                        @master
- agni_manip_utils                                          @master
- object_fitter                                             @master
- sq_fitting                                                @master
- sq_fitting_ros                                            @master
- grasp_visualization                                       @kinetic-devel
- ros-meka_description                                      @master
- ros-meka_bie_moveit_config                                @master
- ros-m3_msgs                                               @master
- ros-m3meka_msgs                                           @master
- ros-tobi_picknplace                                       @kinetic
- ros-tobi_ros4rsb                                          @kinetic
- ros-waving_detection                                      @kinetic
- ros-stage-ros                                             @1.7.4
- gazebo-ros-pkgs-plugins                                   @kinetic-devel
