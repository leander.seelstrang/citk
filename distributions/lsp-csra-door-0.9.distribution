catalog:
  title: CSRA Door
  version: '0.9'

variables:
  access: private
  flavor: xenial
  rstexperimental.exit_on_ambiguity: '1'
  datadir: /vol/csra/data/${variant}/
  variant: '0.9'

  platform-requires:
    ubuntu:
      packages:
      - cmake
      - build-essential
      - libprotobuf-dev
      - protobuf-compiler
      - libboost-dev
      - python-setuptools
      - python-dev
      - python-protobuf
      - libboost-thread-dev
      - libboost-filesystem-dev
      - libboost-signals-dev
      - libboost-program-options-dev
      - libboost-system-dev
      - libboost-regex-dev
      - liblog4cxx10-dev
      - libffi-dev
      - libpcl-dev
      - ocl-icd-opencl-dev
      - opencl-headers

  toolkit.dir: ${toolkit.volume}/lsp-csra-${variant}
  toolkit.volume: /tmp/

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: mkdir -p "${toolkit.dir}"

versions:
- kogniserver                                               @master
- kognihome-displayserver-extras                            @master
- kognidoor-eink                                            @csra
- kognidoor-frontend                                        @csra
- kognihome-doorcontroller                                  @csra
- kognidoor-image-server                                    @csra
- kognidoor-vision                                          @csra
- rsbhsm                                                    @toolkit
- python-transitions                                        @master
- kognidoor-ble-grabber                                     @csra
- kognicom                                                  @master
- kognidoor-dispatcher                                      @csra
- kogniconfig                                               @csra
- lsp-csra-system-startup                                   @master
- lsp-csra-system-config                                    @master
- kognijenkins                                              @master
- icl-kognihome                                             @trunk
- bullet                                                    @master
- vdemo                                                     @master
- spread                                                    @4.4
- spread-python                                             @1.5spread4
- rsc                                                       @0.17
- rsb-protocol                                              @0.17
- rsb-cpp                                                   @0.17
- rsb-python                                                @0.17
- rsb-spread-cpp                                            @0.17
- rst-proto                                                 @0.17
- rst-experimental-proto                                    @0.17
- rst-converters-cpp                                        @0.17
- rst-converters-python                                     @0.17
- rsb-xml-cpp                                               @0.17
- rsb-gstreamer                                             @0.17
- rsb-xml-python                                            @0.17
- rsb-tools-cpp                                             @0.17
- rsb-tools-cl-binary                                       @0.17
- rsbag-tools-cl-binary                                     @0.17
- nodejs                                                    @v8.9.2
- peerjs-server                                             @master
- kognidoor-obstacledetection                               @csra
- kognidoor-lf-grabber                                      @master
