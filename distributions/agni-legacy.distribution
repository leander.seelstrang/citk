catalog:
  title: AGNI 32bit (legacy) distribution
  version: nightly

variables:
  git.clean-before-checkout?: false
  subversion.checkout-strategy: update
  scm.trigger-disable?: ${next-value|${external?|false}}
  junit.allow-empty-results?: true

  email-notification.recipients:
  - rhaschke@techfak.uni-bielefeld.de
  access: private

  ros: ${next-value|melodic}
  ros.install.prefix: ${toolkit.dir}/ros/${ros}

  toolkit.volume: /vol/legacy/\$(lsb_release -sc)
  toolkit.dir: ${toolkit.volume}

  cflags: -m32
  cxxflags: -m32 -std=c++11
  ldflags: -m32 -Wl,--disable-new-dtags

  shell.environment:default:
  - '@{next-value|[]}'
  - CPPFLAGS="-m32 \$CPPFLAGS"
  - LIBRARY_PATH=${toolkit.dir}/lib

  cmake.options:default:
  - '@{next-value|[]}'
  - CMAKE_DISABLE_FIND_PACKAGE_FLTK=TRUE
  - CMAKE_DISABLE_FIND_PACKAGE_QT5=TRUE

  prepare-hook-name: distribution-prepare-${distribution-name}
  prepare-hook/unix: |
    rm -rf "${toolkit.dir}"/*
    for d in include lib bin share; do mkdir -p "${toolkit.dir}/\$d"; done
    cd "${toolkit.dir}/lib"
    @{link.commands}
  finish-hook-name: distribution-finish-${distribution-name}
  finish-hook/unix: chmod -R a+rX,g-w,o-w "${toolkit.dir}"/*

  platform-requires:
    ubuntu:
      packages:
      - '@{next-value|[]}'
      - libdb5.3++:i386
      - libexpat1-dev:i386
      - libbz2-dev:i386
      - libmcpp0:i386
      - libboost-thread-dev:i386
      - libboost-program-options-dev:i386
      - libboost-filesystem-dev:i386
      - libboost-signals-dev:i386
      - libboost-date-time-dev:i386
      - libboost-chrono-dev:i386
      - libboost-atomic-dev:i386
      - libtinyxml-dev:i386
      - libncurses5-dev:i386
      - libssl1.0.0:i386
      - libaprutil1:i386
      - liblog4cxx10v5:i386
      - libglu1-mesa-dev:i386
      - libjpeg-dev:i386
      - libssl-dev:i386
      - libdbus-1-dev:i386
      - libtk8.6:i386
      - libfftw3-dev:i386
      - libxt-dev:i386
      - libxpm-dev:i386
      - libconsole-bridge-dev:i386
      trusty:
        packages:
        - '@{next-value|}'
        - libmuparser2:i386
        - libpng12-dev:i386
        - libxerces-c3.1:i386
      xenial:
        packages:
        - '@{next-value|}'
        - libmuparser2v5:i386
        - libboost-regex-dev:i386
        - libpng12-dev:i386
        - libxerces-c3.1:i386
      bionic:
        packages:
        - '@{next-value|}'
        - libmuparser2v5:i386
        - libboost-regex1.65.1:i386
        - libpng-dev:i386
        - libxerces-c3.2:i386

  links:
  - libmcpp.so.0 libmcpp.so
  - libdb-5.3.so libdb.so
  - libdb_cxx-5.3.so libdb_cxx.so
  - libxerces-c-3.?.so libxerces-c.so
  - liblog4cxx.so.10 liblog4cxx.so
  - libmuparser.so.2 libmuparser.so
  - libboost_regex.so.*.? libboost_regex.so
  link.commands: |
    ln -sf /usr/lib/i386-linux-gnu/${links}

versions:
- ice                                                       @3.4.2
- spread                                                    @4.4
- xqilla                                                    @2.3
- xmltio                                                    @master
- xmltio-ni                                                 @master
- xcf                                                       @master
- boost_threadpool                                          @0.2.5
- xcf-am                                                    @master
- ni-logger                                                 @master
- hsm                                                       @master
- spacenavi                                                 @master
- libSTP                                                    @master
- pa10                                                      @master
- kdl                                                       @master-ni
- cbf                                                       @master
- vtk                                                       @4.2.6
- roscpp_core                                               @kinetic-devel
- rosconsole                                                @melodic-devel
- xmlrpcpp                                                  @melodic-devel
- roscpp                                                    @melodic-devel
- actionlib                                                 @indigo-devel
- ni-arm-server                                             @master
