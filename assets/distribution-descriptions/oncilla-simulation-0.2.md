Oncilla is a light-weight, bio-inspired quadruped robot for fast locomotion, developed in the AMARSi Project.
The Oncilla Simulator contains a Project Wizard, that helps you set up new simulation projects and experiments,
and installs Examples for the usage of the different API layers.

In this particular system the Oncilla Simulation is used to simulate a sine movement. Please consider reading
linked publications and feel free to 1) replicate the system and 2) execute the linked experiment Oncilla-Sim-0.2-Sine
to verify our results. Please see Linked Artifacts section.