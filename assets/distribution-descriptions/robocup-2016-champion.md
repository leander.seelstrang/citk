
The Team of Bielefeld (ToBi) has been founded in 2009. The robocup activities are embedded in a long-term research history
towards human-robot interaction with laypersons in regular home environments. The overall research goal is to provide a
robot with capabilities that enable the interactive teaching of skills and tasks through natural communication in
previously unknown environments. This work is based on the BIRON platform developed in the Applied Informatics Group
within the Home Tour scenario.

The challenge is two-fold. On the one hand, we need to understand the communicative cues of humans and how they
interpret robotic behavior. On the other hand, we need to provide technology that is able to perceive the environment,
detect and recognize humans, navigate in changing environments, localize and manipulate objects, initiate and understand
a spoken dialog. Thus, it is important to go beyond typical command-style interaction and to support mixed-initiative
learning tasks.