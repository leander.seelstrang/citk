The Robobench alpha benchmark is a proof of concept benchmark that demonstrates containerization of  six different missions exercising different frameworks and simulators inside of a container.

These simulations illustrate that whole mission benchmarking inside a container is feasible across many different software environments. However, a number of issues do arise. Among them are finding the right library symbols, helping the profiler find the symbols inside the container, and making sure that the container is configured correctly for native GPU access. 

These missions explore a number of issues that are described in greater depth in the knowledge base.

While this set of simulations is far from comprehensive, a few interesting observations where found in this dataset. From the perspective of power consumption, many tasks that are considered typically computationally expensive are not as important as moderately intensive tasks that run constantly and are safety critical, such as updating occupancy grids for collision avoidance. 