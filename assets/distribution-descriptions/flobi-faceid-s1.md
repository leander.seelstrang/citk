This system is built on top of the Humanoid Robot Head Flobi's Software Stack (cf. Linked Artifacts below).
In this particular setup, Flobi has been trained to recognize subjects based on images that are sent via middleware
camera stream and reacts with a subject-specific emotion [fear, sad, happy, angry | emotions, corresponding to subjects
have been randomly assigned]. Flobi 'knows' Bill, Steve, Dennis and Linus. In case no pysical Flobi hardware is available
the system also comes with a simulated Flobi. Face learning and recognition has been realized using the OpenCV-based
Distributed Face Recognizer.